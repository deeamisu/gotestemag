package services

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"sync"
	"testEmagGo/fakeDb"
)

func PersistDbToFile(db map[string]int, filename string) error {
	jsonData, err := json.Marshal(db)
	if err != nil {
		return err
	}

	mutex := &sync.Mutex{}
	mutex.Lock()

	jsonFile, err := os.Create(filename)

	if err != nil {
		return err
	}
	defer func(jsonFile *os.File) {
		err := jsonFile.Close()
		mutex.Unlock()
		if err != nil {
			panic(err)
		}
	}(jsonFile)

	_, err2 := jsonFile.Write(jsonData)
	if err2 != nil {
		return err2
	}

	return nil
}

func PopulateDb(db map[string]int, filename string) error {
	mutex := &sync.Mutex{}
	mutex.Lock()

	jsonFile, err := os.Open(filename)
	if err != nil {
		if err := createDbFile(mutex); err != nil {
			return err
		}
		return err
	}

	defer func(jsonFile *os.File) {
		err := jsonFile.Close()
		mutex.Unlock()
		if err != nil {
			panic(err)
		}
	}(jsonFile)

	byteValue, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		return err
	}

	err = json.Unmarshal(byteValue, &db)
	if err != nil {
		return err
	}

	return nil
}

func createDbFile(mutex *sync.Mutex) error {

	jsonFile, err := os.Create("FakeDb.json")
	if err != nil {
		return err
	}

	defer func(jsonFile *os.File) {
		err := jsonFile.Close()
		mutex.Unlock()
		if err != nil {
			panic(err)
		}
	}(jsonFile)

	jsonData, err := json.Marshal(fakeDb.FileDb)
	if err != nil {
		return err
	}

	if _, err2 := jsonFile.Write(jsonData); err2 != nil {
		return err2
	}

	return nil
}

func SyncDb(db, fileDb, tmpDB map[string]int, filename string) error {
	err := PopulateDb(fileDb, filename)
	if err != nil {
		return err
	}

	for key := range fileDb {
		db[key] += fileDb[key] - tmpDB[key]
	}

	return nil
}
