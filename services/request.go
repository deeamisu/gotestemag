package services

import (
	"strings"
	"testEmagGo/models"
	"unicode"
)

func ProcessRequest(request *models.RequestModel, fakeDb map[string]int) {
	text := (*request).Text
	words := strings.FieldsFunc(text, getSeparator)
	for _, word := range words {
		fakeDb[strings.ToLower(word)]++
	}
}

func getSeparator(r rune) bool {
	if unicode.IsSpace(r) {
		return true
	}
	switch r {
	case '.':
		return true
	case ',':
		return true
	case '!':
		return true
	case '?':
		return true
	case ';':
		return true
	case ':':
		return true
	default:
		return false
	}
}