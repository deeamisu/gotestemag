package models

type RequestModel struct {
	Text string 	`json:"text" example:"Ana are mere"`
}

type ResponseModel struct {
	Word string 	`json:"word" example:"ana"`
	Occurrences int	`json:"occurrences" example:"2"`
}

type ErrorMessage struct {
	Message string	`json:"message" example:"error message"`
}

type SuccessMessage struct {
	Message string	`json:"message" example:"Success!"`
}