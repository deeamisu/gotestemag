package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"testEmagGo/fakeDb"
	"testEmagGo/logger"
	"testEmagGo/models"
	"testEmagGo/services"
)

// GetCurrencies godoc
// @Summary Get Occurrences
// @Description Retrieves a list of words and occurrences
// @Tags words
// @ID occurrences
// @Produce  json
// @param word query string true "the word to be retrieved"
// @Success 200 {object} models.ResponseModel
// @Failure 400 {object} models.ErrorMessage "the error message"
// @Router /occurrences [get]
func GetCurrencies(w http.ResponseWriter, r *http.Request)  {
	w.Header().Set("Content-Type", "application/json")

	switch r.Method {
	case "GET":
		query := r.URL.Query()
		word := strings.ToLower(strings.Join(query["word"], ""))
		response := models.ResponseModel{
			Word:        word,
			Occurrences: fakeDb.Db[word],
		}

		err := json.NewEncoder(w).Encode(response)
		if err != nil {
			logger.InfoLogger.Fatal(err)
		}

		logger.AccessLogger.Println("GET endpoint accessed")

	default:
		methodNotAllowed(&w)
	}
}

//InsertText godoc
// @Summary Post text
// @Description Inserts words from text in db or increments their occurrences
// @Tags text
// @ID insert
// @accept json
// @Produce  json
// @param text body models.RequestModel true "the text to be processed"
// @Success 201 {object} models.SuccessMessage "confirmation message"
// @Failure 400 {object} models.ErrorMessage "the error message"
// @Router /insert [post]
func InsertText(w http.ResponseWriter, r *http.Request )  {
	var text models.RequestModel

	w.Header().Set("Content-Type", "application/json")

	switch r.Method {
	case "POST":
		err := json.NewDecoder(r.Body).Decode(&text)
		if err != nil {
			logger.InfoLogger.Println(err.Error())
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		services.ProcessRequest(&text, fakeDb.Db)
		response := map[string]string{"message": "Success"}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusCreated)
		sendSimpleResponse(&w, response)

		logger.ModificationLogger.Println(fmt.Sprintf("New text added: %v", text.Text))
		logger.AccessLogger.Println("POST endpoint accessed")

	default:
		methodNotAllowed(&w)
	}
}

func methodNotAllowed(w *http.ResponseWriter) {
	logger.AccessLogger.Println("Wrong attempt")

	(*w).WriteHeader(http.StatusBadRequest)
	message := map[string]string{"message": "No such method allowed"}

	sendSimpleResponse(w, message)
}

func sendSimpleResponse(w *http.ResponseWriter, message map[string]string) {
	err := json.NewEncoder(*w).Encode(message)
	if err != nil {
		logger.InfoLogger.Fatal(err)
	}
}
