package main

import (
	"context"
	"flag"
	"fmt"
	httpSwagger "github.com/swaggo/http-swagger"
	"net/http"
	"os"
	"os/signal"
	"testEmagGo/docs"
	"testEmagGo/fakeDb"
	"testEmagGo/handlers"
	"testEmagGo/logger"
	"testEmagGo/services"
	"time"
)

const FILENAME = "FakeDb.json"

var Port int

func init() {
	logger.InitLogger()

	if err := services.PopulateDb(fakeDb.Db, FILENAME); err != nil {
		logger.InfoLogger.Fatal(err)
	}

	if err := services.PopulateDb(fakeDb.TmpDb, FILENAME); err != nil {
		logger.InfoLogger.Fatal(err)
	}

	flag.IntVar(&Port, "port", 8080,"the port on which to run the app")
}

// @title Fake DB Tryout
// @version 1.0
// @description #doamneajuta!
// @contact.name Mihai Dragnea
// @contact.email misudeea2009@yahoo.com
// @BasePath /
func main() {
	flag.Parse()
	docs.SwaggerInfo.Host = fmt.Sprintf("localhost:%d", Port)

	srv := initializeServer()

	stop := make(chan os.Signal)
	signal.Notify(stop, os.Interrupt)

	ticker := time.NewTicker(5 * time.Second)
	go backup(ticker)

	go func() {
		if err := srv.ListenAndServe(); err != nil {
			if err != http.ErrServerClosed {
				logger.InfoLogger.Println(err)
			}
		}
	}()

	<-stop

	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	if err := services.SyncDb(fakeDb.Db, fakeDb.FileDb, fakeDb.TmpDb, FILENAME); err != nil {
		logger.InfoLogger.Fatal(err)
	}

	if err := services.PersistDbToFile(fakeDb.Db, FILENAME); err != nil {
		logger.InfoLogger.Fatal(err)
	}

	if err := srv.Shutdown(ctx); err != nil {
		logger.InfoLogger.Fatal(err)
	}
}

func initializeServer() *http.Server {
	mux := http.NewServeMux()

	mux.HandleFunc("/occurrences/", handlers.GetCurrencies)
	mux.HandleFunc("/insert", handlers.InsertText)
	mux.HandleFunc("/swagger/", httpSwagger.WrapHandler)

	srv := &http.Server{
		Addr:    fmt.Sprintf(":%d", Port),
		Handler: mux,
	}

	return srv
}

func backup(ticker *time.Ticker) {
	for {
		select {
		case <-ticker.C:
			if err := services.SyncDb(fakeDb.Db, fakeDb.FileDb, fakeDb.TmpDb, FILENAME); err != nil {
				logger.InfoLogger.Fatal(err)
			}

			if err := services.PersistDbToFile(fakeDb.Db, FILENAME); err != nil {
				logger.InfoLogger.Fatal(err)
			}

			if err := services.PopulateDb(fakeDb.TmpDb, FILENAME); err != nil {
				logger.InfoLogger.Fatal(err)
			}
		}
	}
}
