package tests

import (
	"io/ioutil"
	"os"
	"testEmagGo/models"
	"testEmagGo/services"
	"testing"
)

func TestProcessRequest(t *testing.T) {
	request := models.RequestModel{Text: "Ana, are ana mere"}
	db := make(map[string]int)

	services.ProcessRequest(&request, db)

	if db["ana"] != 2 {
		t.Errorf("Expected 2, received %d", db["ana"])
	}
}

func BenchmarkProcessRequest(b *testing.B) {
	request := models.RequestModel{Text: "Ana, are ana mere"}
	db := make(map[string]int)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		services.ProcessRequest(&request, db)
	}
}

func TestPopulateDb(t *testing.T) {
	err := createPopulateDbJsonTestFile()
	if err != nil {
		t.Error(err)
	}

	db := make(map[string]int)
	err = services.PopulateDb(db, "testDB.json")
	if err != nil {
		t.Error(err)
	}

	if db["ana"] != 3 {
		t.Errorf("Expected 3, received %d", db["ana"])
	}

	err = os.Remove("testDB.json")
	if err != nil {
		t.Error(err)
	}
}

func createPopulateDbJsonTestFile() error {
	f, err := os.Create("testDB.json")
	if err != nil {
		return err
	}

	_, err = f.WriteString("{\"ana\":3,\"are\":1}")
	if err != nil {
		return err
	}

	err = f.Close()
	if err != nil {
		return err
	}
	return nil
}

func BenchmarkPopulateDb(b *testing.B) {
	db := make(map[string]int)
	err := createPopulateDbJsonTestFile()
	if err != nil {
		b.Error(err)
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		err := services.PopulateDb(db, "testDB.json")
		if err != nil {
			b.Error(err)
		}
	}

	err = os.Remove("testDB.json")
	if err != nil {
		b.Error(err)
	}
}

func TestPersistDbToFile(t *testing.T) {
	var db = map[string]int{"ana": 4, "marius": 3}
	err := services.PersistDbToFile(db, "tmpDbTest.json")
	if err != nil {
		t.Error(err)
	}

	b, err := ioutil.ReadFile("tmpDbTest.json") // just pass the file name
	if err != nil {
		t.Error(err)
	}

	s := string(b)
	if s != `{"ana":4,"marius":3}` {
		t.Errorf(`Expected {"ana":4,"marius":3}, got %s`, s)
	}
	err = os.Remove("tmpDbTest.json")
	if err != nil {
		t.Error(err)
	}
}

func BenchmarkPersistDbToFile(b *testing.B) {
	var db = map[string]int{"ana": 4, "marius": 3}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		err := services.PersistDbToFile(db, "tmpDbTest.json")
		if err != nil {
			b.Error(err)
		}
	}

	err := os.Remove("tmpDbTest.json")
	if err != nil {
		b.Error(err)
	}
}

func TestSyncDb(t *testing.T) {
	db := map[string]int{"ana":3, "mihai":2}
	fileDb := map[string]int{}
	tmpDb := map[string]int{"ana":1}

	err := createPopulateDbJsonTestFile()
	if err != nil {
		t.Error(err)
	}

	err = services.SyncDb(db, fileDb, tmpDb, "testDB.json")
	if err != nil {
		t.Error(err)
	}

	if db["ana"] != 5 {
		t.Errorf("Expected 5, received %d", db["ana"])
	}
	if db["are"] != 1 {
		t.Errorf("Expected 1, received %d", db["ana"])
	}
	if db["mihai"] != 2 {
		t.Errorf("Expected 2, received %d", db["ana"])
	}

	err = os.Remove("testDB.json")
	if err != nil {
		t.Error(err)
	}
}

func BenchmarkSyncDb(b *testing.B) {
	db := map[string]int{"ana":3, "mihai":2}
	fileDb := map[string]int{}
	tmpDb := map[string]int{"ana":1}

	err := createPopulateDbJsonTestFile()
	if err != nil {
		b.Error(err)
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		err = services.SyncDb(db, fileDb, tmpDb, "testDB.json")
		if err != nil {
			b.Error(err)
		}
	}

	err = os.Remove("testDB.json")
	if err != nil {
		b.Error(err)
	}
}
