package tests

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testEmagGo/handlers"
	"testEmagGo/logger"
	"testEmagGo/models"
	"testing"
)

func TestMain(m *testing.M) {
	initTestLogger()

	exitVal := m.Run()

	os.Exit(exitVal)
}

func initTestLogger()  {

	var buffer bytes.Buffer

	logger.InfoLogger = log.New(&buffer, "INFO: ", log.Ldate|log.Ltime|log.Lshortfile)
	logger.AccessLogger = log.New(&buffer, "ACCESS: ", log.Ldate|log.Ltime|log.Lshortfile)
	logger.ModificationLogger = log.New(&buffer, "MODIFIED: ", log.Ldate|log.Ltime|log.Lshortfile)
}

func TestGetCurrencies(t *testing.T) {
	request, err := http.NewRequest("GET", "/occurrences", nil)
	if err != nil {
		t.Fatal(err)
	}
	
	responseRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(handlers.GetCurrencies)
	handler.ServeHTTP(responseRecorder, request)
	
	if status := responseRecorder.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
}

func TestImportText(t *testing.T)  {
	var requestBody = models.RequestModel{Text: "Mihai are mere"}

	jsonData, err := json.Marshal(requestBody)
	if err != nil {
		t.Fatal(err)
	}

	request, err := http.NewRequest("POST", "/entry", bytes.NewBuffer(jsonData))
	if err != nil {
		t.Fatal(err)
	}
	request.Header.Set("Content-Type", "application/json")

	responseRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(handlers.InsertText)
	handler.ServeHTTP(responseRecorder, request)

	if status := responseRecorder.Code; status != http.StatusCreated {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusCreated)
	}
}