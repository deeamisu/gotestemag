package logger

import (
	"log"
	"os"
)

var (
	AccessLogger *log.Logger
	InfoLogger    *log.Logger
	ModificationLogger   *log.Logger
)

func InitLogger() {
	infoFile, err := os.OpenFile("info_logs.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatal(err)
	}
	accessFile, err := os.OpenFile("access_logs.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatal(err)
	}
	modifiedFile, err := os.OpenFile("modifications_logs.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatal(err)
	}

	InfoLogger = log.New(infoFile, "INFO: ", log.Ldate|log.Ltime|log.Lshortfile)
	AccessLogger = log.New(accessFile, "ACCESS: ", log.Ldate|log.Ltime|log.Lshortfile)
	ModificationLogger = log.New(modifiedFile, "MODIFIED: ", log.Ldate|log.Ltime|log.Lshortfile)
}

